/************************************************************************************
 * Created at 2018/1/5                                                                                 
 * 有贝科技                                                                      
 *
 * 项目名称：数据中心体系v2.0                                                          
 * 版权说明：本软件属有贝科技所有，在未获得有贝科技正式授权情况下，任何企业和   
 *	  个人，不能获取、阅读、安装、传播本软件涉及的任何受知识产权保护的    
 *	  内容。                            
 ************************************************************************************/
package com.cgblog.server.utils;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.*;

/**
 * @author ChenGang
 * @version 2.0
 * @Package com.cgsj.utils
 * @Description: 博客内容的文件形式存储与读取
 * @date 一月 05 2018,20:01
 */

@Service("fileIoUtils")
public class FileIoUtils {

    private static Logger logger = LoggerFactory.getLogger("FileIoUtils");


    /**
     * 根据文章的id从文件系统获取文章内容
     *
     * @param articlePath
     * @return
     */
    public static String getContent(String articlePath) throws IOException {
        StringBuffer content = new StringBuffer();
        File file = new File(articlePath);
        FileInputStream stream = new FileInputStream(file);
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        try {
            String hasRead = null;
            while ((hasRead = reader.readLine()) != null) {
                content.append(hasRead);
                content.append("\n");
            }
        } catch (Exception e) {
            logger.error("获取{}的文件内容异常:{}", articlePath, ExceptionUtils.getFullStackTrace(e));
            throw e;
        } finally {
            reader.close();
            stream.close();
        }
        return content.toString();
    }


    /**
     * 存储编辑后博客文章
     *
     * @param articlePath
     * @return
     */
    public static Boolean storeContent(String articlePath, String content) throws IOException {
        boolean store = false;
        File file = new File(articlePath);
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            FileOutputStream outputStream = new FileOutputStream(file);
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream));
            writer.write(content);
            store = true;
            writer.close();
            outputStream.close();
        } catch (Exception e) {
            logger.error("存储{}的文件内容异常:{}", articlePath, ExceptionUtils.getFullStackTrace(e));
            throw e;
        }
        return store;
    }


    /**
     * 删除指定路径的博客文件
     *
     * @param articlePath
     * @return
     */
    public static Boolean deleteArticle(String articlePath) {
        boolean delete = true;
        File file = new File(articlePath);
        try {
            if (file.exists()) {
                delete = file.delete();
            }
        } catch (Exception e) {
            delete = false;
            logger.error("删除{}的文件异常:{}", articlePath, ExceptionUtils.getFullStackTrace(e));
        }
        return delete;
    }

    /**
     * 移动文件到指定文件夹
     *
     * @return
     */
    public static Boolean moveArticle(String oldParh, String newPath) {
        boolean res = false;
        try {
            File file = new File(oldParh);
            res = file.renameTo(new File(newPath));
        } catch (Exception e) {
            logger.error("移动{}的文件到{}异常:{}", oldParh, newPath, ExceptionUtils.getFullStackTrace(e));
        }
        return res;

    }


    public static void main(String[] args) throws IOException {
        String str1 = "C:\\Users\\83771\\Desktop\\BlogTest\\design\\777.txt";
        String str2 = "C:\\Users\\83771\\Desktop\\BlogTest\\test\\777.txt";
        moveArticle(str1, str2);
    }


}
