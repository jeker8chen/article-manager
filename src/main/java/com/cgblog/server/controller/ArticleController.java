/************************************************************************************
 * Created at 2018/1/6                                                                                 
 * 有贝科技                                                                      
 *
 * 项目名称：数据中心体系v2.0                                                          
 * 版权说明：本软件属有贝科技所有，在未获得有贝科技正式授权情况下，任何企业和   
 *	  个人，不能获取、阅读、安装、传播本软件涉及的任何受知识产权保护的    
 *	  内容。                            
 ************************************************************************************/
package com.cgblog.server.controller;

import com.cgblog.domain.BlogArticle;
import com.cgblog.server.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author ChenGang
 * @version 2.0
 * @Package com.cgblog.controller
 * @Description: 博客文件对外接口
 * @date 一月 06 2018,13:35
 */

@RestController
@RequestMapping(value = "/article")
public class ArticleController {

    @Resource
    private ArticleService articleService;

    @RequestMapping("/test")
    @ResponseBody
    private String testTest(){
        return "article-manager test";
    }

    @GetMapping(value = "/get")
    @ResponseBody
    public String getArticle(@ModelAttribute BlogArticle article) {
        String content = articleService.getArticle(article);
        return content;
    }

    @RequestMapping(value = "/store")
    public Boolean storeArticle(@ModelAttribute BlogArticle article
            , @RequestBody String articleContent) {
        boolean result = articleService.storeArticle(article, articleContent);
        return result;
    }

    @GetMapping(value = "/delete")
    public Boolean deleteArticle(@ModelAttribute BlogArticle article) {
        boolean result = articleService.deleteArticle(article);
        return result;
    }

    @GetMapping(value = "/changeType")
    public Boolean changeType(@ModelAttribute BlogArticle article, @RequestParam("lastType") String lastType) {
        boolean result = articleService.moveArticle(article, lastType);
        return result;
    }


}
