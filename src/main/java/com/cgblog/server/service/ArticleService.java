/************************************************************************************
 * Created at 2018/1/6                                                                                 
 * 有贝科技                                                                      
 *
 * 项目名称：数据中心体系v2.0                                                          
 * 版权说明：本软件属有贝科技所有，在未获得有贝科技正式授权情况下，任何企业和   
 *	  个人，不能获取、阅读、安装、传播本软件涉及的任何受知识产权保护的    
 *	  内容。                            
 ************************************************************************************/
package com.cgblog.server.service;

import com.cgblog.domain.BlogArticle;

/**
 * @author ChenGang
 * @version 2.0
 * @Package com.cgblog.service
 * @Description: (这里用一句话描述这个类的作用)
 * @date 一月 06 2018,13:37
 */
public interface ArticleService {

    String getArticle(BlogArticle article);

    Boolean storeArticle(BlogArticle article, String content);

    Boolean deleteArticle(BlogArticle article);

    Boolean moveArticle(BlogArticle article,String lastType);


}
