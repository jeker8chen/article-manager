/************************************************************************************
 * Created at 2018/1/6                                                                                 
 * 有贝科技                                                                      
 *
 * 项目名称：数据中心体系v2.0                                                          
 * 版权说明：本软件属有贝科技所有，在未获得有贝科技正式授权情况下，任何企业和   
 *	  个人，不能获取、阅读、安装、传播本软件涉及的任何受知识产权保护的    
 *	  内容。                            
 ************************************************************************************/
package com.cgblog.server.service.impl;

import com.cgblog.domain.BlogArticle;
import com.cgblog.server.service.ArticleService;
import com.cgblog.server.utils.FileIoUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author ChenGang
 * @version 2.0
 * @Package com.cgblog.service.impl
 * @Description: 实现类
 * @date 一月 06 2018,13:37
 */

@Service("articleService")
public class ArticleServiceImpl implements ArticleService {
    private static Logger logger = LoggerFactory.getLogger("ArticleService");

    @Value("${article.path}")
    private String articlePath;

    @Value("${article.file.suffix}")
    private String fileSuffix;

    @Override
    public String getArticle(BlogArticle article) {
        String articleContent = null;
        String realPath = composePath(article);
        try {
            articleContent = FileIoUtils.getContent(realPath);
        } catch (Exception e) {
            logger.error("获取博客文章[id={}]异常：{}", article.getId(), ExceptionUtils.getFullStackTrace(e));
        }
        return articleContent;
    }

    @Override
    public Boolean storeArticle(BlogArticle article, String content) {
        boolean result = false;
        String realPath = composePath(article);
        try {
            result = FileIoUtils.storeContent(realPath, content);
        } catch (Exception e) {
            logger.error("存储博客文章[id={}]异常：{}", article.getId(), ExceptionUtils.getFullStackTrace(e));
        }
        return result;
    }

    @Override
    public Boolean deleteArticle(BlogArticle article) {
        boolean result = false;
        try {
            String realPath = composePath(article);
            result = FileIoUtils.deleteArticle(realPath);
        } catch (Exception e) {
            logger.error("删除博客文章[id={}]异常：{}", article.getId(), ExceptionUtils.getFullStackTrace(e));
        }
        return result;
    }

    @Override
    public Boolean moveArticle(BlogArticle article, String lastType) {
        boolean result = false;
        try {
            String newPath = composePath(article);
            article.setArticleType(lastType);
            String oldPath = composePath(article);
            result = FileIoUtils.moveArticle(oldPath, newPath);
        } catch (Exception e) {
            logger.error("移动博客文章[id={}]异常：{}", article.getId(), ExceptionUtils.getFullStackTrace(e));
        }
        return result;
    }


    /**
     * 合成博客的路径
     *
     * @param article
     * @return
     */
    private String composePath(BlogArticle article) {
//        return articlePath + "\\" + article.getArticleType() + "\\" + article.getId() + fileSuffix;
        return articlePath + "/" + article.getArticleType() + "/" + article.getId() + fileSuffix;
    }

}
